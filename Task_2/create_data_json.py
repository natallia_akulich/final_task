import json

from datetime import datetime
number = datetime.now().strftime('%Y%m%d%H%M%S%f')

data = {'auth_token': 'r7dRW1aPiX2KQJyKx7O9yJBvU6BTXRDMxOYS',
        'users_url': 'https://gorest.co.in/public-api/users',
        'user': {"first_name": "Brianna", "last_name": "Ratke", "gender": "male",
                 "email": f"testuser{number}@testoberts.com", "status": "active"},
        'user_new_last_name': {"last_name": "Test"},
        'all_users_amount': 20}


with open('data.json', 'w') as file:
    json.dump(data, file, indent=4)


