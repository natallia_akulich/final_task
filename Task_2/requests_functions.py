import requests
from requests import HTTPError
import pytest


def get_request(url, token):
    try:
        response = requests.get(url, headers={"Authorization": f"Bearer {token}"})
        response.raise_for_status()
    except HTTPError as http_err:
        pytest.fail(f'HTTP error occurred: {http_err}')
    else:
        return response


def receive_get_body(url, token, expected_status=200):
    response = get_request(url, token)
    body = response.json()
    meta_body = body['_meta']
    if meta_body['code'] == expected_status:
        return body
    else:
        pytest.fail(f'{meta_body["code"]} {meta_body["message"]}')


def get_all_users(url, token):
    response = receive_get_body(url, token)
    if response:
        return response['result']


def get_all_users_bad_token(url, token=''):
    response = receive_get_body(url, token, expected_status=401)
    return response['_meta']


def get_user_data_by_id(url, token, user_id):
    response = receive_get_body(f'{url}/{user_id}', token)
    if response:
        return response['result']


def post_request(url, token, data):
    try:
        response = requests.post(url, headers={"Authorization": f"Bearer {token}"}, json=data)
        response.raise_for_status()
    except HTTPError as http_err:
        pytest.fail(f'HTTP error occurred: {http_err}')
    else:
        return response


def receive_post_body(url, token, data, expected_status=200):
    response = post_request(url, token, data)
    body = response.json()
    meta_body = body['_meta']
    result_body = body['result']
    if meta_body['code'] == expected_status:
        return body
    else:
        pytest.fail(f'{meta_body["code"]} {meta_body["message"]}\n'
                    f'{result_body}')


def add_user(url, data, token):
    response = receive_post_body(url, data, token)
    if response:
        result = response['result']
        return result['id']


def add_user_bad_token(url, data, token=''):
    response = receive_post_body(url, data, token, expected_status=401)
    return response['_meta']


def add_user_bad_data(url, token, data=''):
    response = receive_post_body(url, token, data, expected_status=422)
    return response['_meta']['code'], response['_meta']['message']


def put_request(url, token, data):
    try:
        response = requests.put(url, headers={"Authorization": f"Bearer {token}"}, json=data)
        response.raise_for_status()
    except HTTPError as http_err:
        pytest.fail(f'HTTP error occurred: {http_err}')
    else:
        return response


def receive_put_body(url, token, data, expected_status=200):
    response = put_request(url, token, data)
    body = response.json()
    meta_body = body['_meta']
    result_body = body['result']
    if meta_body['code'] == expected_status:
        return body
    else:
        pytest.fail(f'{meta_body["code"]} {meta_body["message"]}\n'
                    f'{result_body}')


def update_user_data_by_id(url, some_id, token, data):
    response = receive_put_body(f'{url}/{some_id}', token, data)
    if response:
        result = response['result']
        return result


def update_user_bad_token(url, some_id, data, token=''):
    response = receive_put_body(f'{url}/{some_id}', data, token, expected_status=401)
    return response['_meta']


def delete_request(url, token):
    try:
        response = requests.delete(url, headers={"Authorization": f"Bearer {token}"})
        response.raise_for_status()
    except HTTPError as http_err:
        pytest.fail(f'HTTP error occurred: {http_err}')
    else:
        return response


def receive_delete_body(url, token, expected_status=204):
    response = delete_request(url, token)
    body = response.json()
    meta_body = body['_meta']
    result_body = body['result']
    if meta_body['code'] == expected_status:
        return True
    else:
        pytest.fail(f'{meta_body["code"]} {meta_body["message"]}\n'
                    f'{result_body}')


def delete_user_by_id(url, some_id, token):
    response = receive_delete_body(f'{url}/{some_id}', token)
    if response:
        return True
