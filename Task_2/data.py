import json
from datetime import datetime


def get_test_data():
    with open("data.json", "r") as read_file:
        number = datetime.now().strftime('%Y%m%d%H%M%S%f')
        test_data = json.load(read_file)
        test_data['user']['email'] = test_data['user']['email'].format(number=number)
        return test_data
