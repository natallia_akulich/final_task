import pytest
from Task_2.data import get_test_data
from Task_2.requests_functions import add_user, delete_user_by_id


@pytest.fixture(scope='function')
def create_test_user():
    data = get_test_data()
    new_user_id = add_user(data['users_url'], data['auth_token'], data['user'])
    yield new_user_id
    delete_user_by_id(data['users_url'], new_user_id, data['auth_token'])


@pytest.fixture()
def data():
    return get_test_data()
