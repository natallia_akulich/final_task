import Task_2.requests_functions as rf


class TestRequests:
    def test_all_users_are_received(self, data):
        result = rf.get_all_users(data['users_url'], data['auth_token'])
        assert len(result) == data['all_users_amount']

    def test_get_user_data_by_id_correct_data(self, create_test_user, data):
        result = rf.get_user_data_by_id(data['users_url'], data['auth_token'], create_test_user)
        assert result['first_name'] == 'Brianna'
        assert result['last_name'] == 'Ratke'

    def test_get_all_users_bad_token(self, data):
        result = rf.get_all_users_bad_token(data['users_url'], token='')
        assert result == {'success': False, 'code': 401, 'message': 'Authentication failed.'}

    def test_add_user_success(self, create_test_user):
        assert type(create_test_user) == str

    def test_add_user_bad_token(self, data):
        result = rf.add_user_bad_token(data['users_url'], data['user'], token='')
        assert result == {'success': False, 'code': 401, 'message': 'Authentication failed.'}

    def test_add_user_bad_data(self, data):
        result = rf.add_user_bad_data(data['users_url'], data['auth_token'], data='')
        assert result == (422, 'Data validation failed. Please check the response body for detailed error messages.')

    def test_update_user_data_by_id(self, create_test_user, data):
        result = rf.update_user_data_by_id(
            data['users_url'], create_test_user, data['auth_token'], data['user_new_last_name'])
        assert result['last_name'] == data['user_new_last_name']['last_name']

    def test_update_user_data_by_id_empty_data(self, create_test_user, data):
        result = rf.update_user_data_by_id(data['users_url'], create_test_user, data['auth_token'], '')
        assert result['last_name'] == data['user']['last_name']

    def test_update_user_bad_token(self, create_test_user, data):
        result = rf.update_user_bad_token(data['users_url'], create_test_user, data['user_new_last_name'], token='')
        assert result == {'success': False, 'code': 401, 'message': 'Authentication failed.'}

    def test_delete_user_by_id(self, data):
        new_user_id = rf.add_user(data['users_url'], data['auth_token'], data['user'])
        result = rf.delete_user_by_id(data['users_url'], new_user_id, data['auth_token'])
        assert result is True
