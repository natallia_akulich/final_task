from Task_1.locators.login_page_locators import LoginPageLocators
from Task_1.pages.base_page import BasePage
import pytest

user_email = 'python.rulezz@gmail.com'
user_password = 'Qa123456789'


class LoginPage(BasePage):

    def should_be_login_page(self):
        self.should_be_login_url()
        self.should_be_login_form_full()
        self.should_be_register_form_full()

    def should_be_login_form_full(self):
        self.should_be_login_form()
        self.should_be_login_form_email()
        self.should_be_login_form_password()
        self.should_be_login_form_forgot()
        self.should_be_login_form_sign_in()

    def should_be_register_form_full(self):
        self.should_be_register_form()
        self.should_be_register_form_email()
        self.should_be_register_form_create_account()
        self.should_be_login_form_email()
        self.should_be_register_form_create_account()

    def login(self):
        self.should_be_login_form_email().send_keys(user_email)
        self.should_be_login_form_password().send_keys(user_password)
        self.should_be_login_form_sign_in().click()

    def login_failed_empty_fields(self):
        self.should_be_login_form_sign_in().click()
        self.auth_failed()

    def login_failed_empty_password(self):
        self.should_be_login_form_email().send_keys(user_email)
        self.should_be_login_form_sign_in().click()
        self.auth_alert_no_password()

    def should_be_login_url(self):
        assert 'controller=authentication&back=my-account' \
               in self.driver.current_url

    def should_be_login_form(self):
        login_form = self.is_element_present(*LoginPageLocators.LOGIN_FORM)
        if not login_form:
            pytest.fail('No element found')
        else:
            return login_form

    def should_be_register_form(self):
        register_form = self.is_element_present(
            *LoginPageLocators.REGISTER_FORM)
        if not register_form:
            pytest.fail('No element found')
        else:
            return register_form

    def should_be_register_form_email(self):
        register_form_email = self.is_element_present(
            *LoginPageLocators.REGISTER_FORM_EMAIL_FIELD)
        if not register_form_email:
            pytest.fail('No element found')
        else:
            return register_form_email

    def should_be_register_form_create_account(self):
        register_form_create_account = self.is_element_present(
            *LoginPageLocators.REGISTER_FORM_CREATE_BUTTON)
        if not register_form_create_account:
            pytest.fail('No element found')
        else:
            return register_form_create_account

    def should_be_login_form_email(self):
        login_form_email = self.is_element_present(
            *LoginPageLocators.LOGIN_FORM_EMAIL_FIELD)
        if not login_form_email:
            pytest.fail('No element found')
        else:
            return login_form_email

    def should_be_login_form_password(self):
        login_form_password = self.is_element_present(
            *LoginPageLocators.LOGIN_FORM_PASSWORD_FIELD)
        if not login_form_password:
            pytest.fail('No element found')
        else:
            return login_form_password

    def should_be_login_form_forgot(self):
        login_form_forgot = self.is_element_present(
            *LoginPageLocators.LOGIN_FORM_FORGOT_PASSWORD)
        if not login_form_forgot:
            pytest.fail('No element found')
        else:
            return login_form_forgot

    def should_be_login_form_sign_in(self):
        login_form_sign_in = self.is_element_present(
            *LoginPageLocators.LOGIN_FORM_SIGN_BUTTON)
        if not login_form_sign_in:
            pytest.fail('No element found')
        else:
            return login_form_sign_in

    def auth_alert_no_email(self):
        alert_one_error = self.is_text_present(*LoginPageLocators.ALERT_ONE_ERROR_TEXT)
        alert_no_email = self.is_text_present(*LoginPageLocators.ALERT_EMAIL_ERROR_TEXT)
        if not alert_one_error and alert_no_email:
            pytest.fail('No text found')
        else:
            return alert_no_email

    def auth_alert_no_password(self):
        alert_one_error = self.is_text_present(*LoginPageLocators.ALERT_ONE_ERROR_TEXT)
        alert_no_password = self.is_text_present(*LoginPageLocators.ALERT_PASSWORD_ERROR_TEXT)
        if not alert_one_error and alert_no_password:
            pytest.fail('No text found')
        else:
            return alert_no_password

    def auth_failed(self):
        alert_one_error = self.is_text_present(*LoginPageLocators.ALERT_ONE_ERROR_TEXT)
        alert_auth_failed = self.is_text_present(*LoginPageLocators.ALERT_AUTH_FAILED)
        if not alert_one_error and alert_auth_failed:
            pytest.fail('No text found')
        else:
            return alert_auth_failed
