from Task_1.locators.main_page_locators import MainPageLocators
from Task_1.pages.base_page import BasePage
import pytest


class MainPage(BasePage):

    def should_be_main_page(self):
        self.should_be_login_link()
        self.should_be_cart_link()
        self.should_be_contact_us_link()
        self.should_be_search_field()
        self.should_be_search_button()
        self.should_be_top_menu()
        self.should_be_popular_link()
        self.should_be_best_sellers_link()

    def open_login_page(self):
        login_link = self.should_be_login_link()
        login_link.click()

    def open_cart_page(self):
        cart_link = self.should_be_cart_link()
        cart_link.click()

    def should_be_login_link(self):
        login_link = self.is_element_present(*MainPageLocators.LOGIN_LINK)
        if not login_link:
            pytest.fail('No element found')
        else:
            return login_link

    def should_be_cart_link(self):
        cart_link = self.is_element_present(*MainPageLocators.CART_LINK)
        if not cart_link:
            pytest.fail('No element found')
        else:
            return cart_link

    def should_be_contact_us_link(self):
        contact_us_link = self.is_element_present(
            *MainPageLocators.CONTACT_US_LINK)
        if not contact_us_link:
            pytest.fail('No element found')
        else:
            return contact_us_link

    def should_be_search_field(self):
        search_field = self.is_element_present(*MainPageLocators.SEARCH_FIELD)
        if not search_field:
            pytest.fail('No element found')
        else:
            return search_field

    def should_be_search_button(self):
        search_button = self.is_element_present(
            *MainPageLocators.SEARCH_BUTTON)
        if not search_button:
            pytest.fail('No element found')
        else:
            return search_button

    def should_be_top_menu(self):
        top_menu = self.is_element_present(*MainPageLocators.TOP_MENU)
        if not top_menu:
            pytest.fail('No element found')
        else:
            return top_menu

    def should_be_popular_link(self):
        popular_link = self.is_element_present(*MainPageLocators.POPULAR_LINK)
        if not popular_link:
            pytest.fail('No element found')
        else:
            return popular_link

    def should_be_best_sellers_link(self):
        best_sellers_link = self.is_element_present(
            *MainPageLocators.BEST_SELLERS_LINK)
        if not best_sellers_link:
            pytest.fail('No element found')
        else:
            return best_sellers_link

    def model_4_link(self):
        model_4_link = self.is_element_present(
            *MainPageLocators.MODEL_4_LINK)
        if not model_4_link:
            pytest.fail('No element found')
        else:
            return model_4_link

    def select_model_4(self):
        model_link = self.model_4_link()
        model_link.click()
