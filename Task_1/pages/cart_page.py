from Task_1.locators.cart_page_locators import CartPageLocators
from Task_1.pages.base_page import BasePage
import pytest


class CartPage(BasePage):

    def should_be_cart_page(self):
        self.should_be_cart_url()
        self.should_be_shopping_steps_order()

    def empty_cart(self):
        self.should_be_empty_warning()
        self.should_be_zero_amount()

    def should_be_cart_url(self):
        assert 'controller=order' in self.driver.current_url

    def should_be_empty_warning(self):
        empty_warning = self.is_text_present(
            *CartPageLocators.EMPTY_CART_WARNING)
        if not empty_warning:
            pytest.fail('No text found')
        else:
            return empty_warning

    def should_be_zero_amount(self):
        zero_amount = self.is_text_present(
            *CartPageLocators.CART_ITEMS_ZERO_AMOUNT)
        if not zero_amount:
            pytest.fail('No text found')
        else:
            return zero_amount

    def should_be_shopping_steps_order(self):
        first_step = self.is_element_present(*CartPageLocators.SUMMARY_STEP)
        second_step = self.is_element_present(*CartPageLocators.SIGN_IN_STEP)
        third_step = self.is_element_present(*CartPageLocators.ADDRESS_STEP)
        fourth_step = self.is_element_present(*CartPageLocators.SHIPPING_STEP)
        last_step = self.is_element_present(*CartPageLocators.PAYMENT_STEP)
        if not first_step and second_step and third_step and fourth_step and last_step:
            pytest.fail('No element found')
        else:
            return first_step, second_step, third_step, fourth_step, last_step

    def should_be_product_table_if_not_empty(self):
        product = self.is_element_present(*CartPageLocators.TABLE_PRODUCT)
        description = self.is_element_present(*CartPageLocators.TABLE_DESCRIPTION)
        available = self.is_element_present(*CartPageLocators.TABLE_AVAILABLE)
        unit_price = self.is_element_present(*CartPageLocators.TABLE_UNIT_PRICE)
        quantitity = self.is_element_present(*CartPageLocators.TABLE_QUANTITY)
        total = self.is_element_present(*CartPageLocators.TABLE_TOTAL)
        delete = self.is_element_present(*CartPageLocators.TABLE_DELETE)
        if not product and description and available and unit_price and quantitity and total and delete:
            pytest.fail('No element found')
        else:
            return product, description, available, unit_price, quantitity, total, delete

    def model_4_added_to_cart(self):
        model_4_added = self.is_text_present(
            *CartPageLocators.MODEL_4_IN_CART)
        if not model_4_added:
            pytest.fail('No text found')
        else:
            return model_4_added

