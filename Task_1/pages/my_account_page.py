from Task_1.locators.my_account_page_locators import MyAccountPageLocators
from Task_1.pages.base_page import BasePage
import pytest


class MyAccountPage(BasePage):

    def should_be_my_account_page(self):
        self.should_be_my_account_url()
        self.should_be_my_account_info_text()
        self.should_be_my_account_title()

    def should_be_my_account_manage(self):
        self.should_be_order_history_button()
        self.should_be_my_credit_slips_button()
        self.should_be_my_addresses_button()
        self.should_be_my_personal_info_button()
        self.should_be_my_wishlists_button()

    def should_be_my_account_url(self):
        assert 'controller=my-account' \
               in self.driver.current_url

    def should_be_my_account_title(self):
        my_account_title = self.is_text_present(*MyAccountPageLocators.MY_ACCOUNT_HEADING)
        if not my_account_title:
            pytest.fail('No text found')
        else:
            return my_account_title

    def should_be_my_account_info_text(self):
        info_text = self.is_text_present(*MyAccountPageLocators.ACCOUNT_INFO_TEXT)
        if not info_text:
            pytest.fail('No text found')
        else:
            return info_text

    def should_be_order_history_button(self):
        order_history_button = self.is_text_present(*MyAccountPageLocators.ORDER_HISTORY_BUTTON)
        if not order_history_button:
            pytest.fail('No text found')
        else:
            return order_history_button

    def should_be_my_credit_slips_button(self):
        my_credit_slips_button = self.is_text_present(*MyAccountPageLocators.MY_CREDIT_SLIPS_BUTTON)
        if not my_credit_slips_button:
            pytest.fail('No text found')
        else:
            return my_credit_slips_button

    def should_be_my_addresses_button(self):
        my_addresses = self.is_text_present(*MyAccountPageLocators.MY_ADDRESSES_BUTTON)
        if not my_addresses:
            pytest.fail('No text found')
        else:
            return my_addresses

    def should_be_my_personal_info_button(self):
        my_personal_info_button = self.is_text_present(*MyAccountPageLocators.MY_PERSONAL_INFORMATION_BUTTON)
        if not my_personal_info_button:
            pytest.fail('No text found')
        else:
            return my_personal_info_button

    def should_be_my_wishlists_button(self):
        my_wishlists_button = self.is_text_present(*MyAccountPageLocators.MY_WISHLISTS_BUTTON)
        if not my_wishlists_button:
            pytest.fail('No text found')
        else:
            return my_wishlists_button

    def should_be_return_home_button(self):
        return_home_button = self.is_element_present(*MyAccountPageLocators.RETURN_HOME_BUTTON)
        if not return_home_button:
            pytest.fail('No text found')
        else:
            return return_home_button

    def return_home(self):
        return_home = self.should_be_return_home_button()
        return_home.click()



