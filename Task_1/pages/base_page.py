from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.common.exceptions import TimeoutException


class BasePage:
    def __init__(self, driver, url):
        self.url = url
        self.driver = driver
        self.driver.implicitly_wait(5)

    def open(self):
        self.driver.get(self.url)

    def is_element_present(self, locate_by, selector, timer=10):
        try:
            return WebDriverWait(self.driver, timer).until(
                ec.presence_of_element_located((locate_by, selector)))
        except TimeoutException:
            return False

    def is_text_present(self, locate_by, selector, text, timer=10):
        try:
            return WebDriverWait(self.driver, timer).until(
                ec.text_to_be_present_in_element((locate_by, selector), text))
        except TimeoutException:
            return False
