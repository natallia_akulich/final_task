from Task_1.locators.model_details_locators import ModelPageLocators
from Task_1.pages.base_page import BasePage
import pytest


class ModelPage(BasePage):

    def should_be_model_page(self):
        self.should_be_model_url()
        self.should_be_model_details()
        self.should_be_order_details()

    def should_be_model_details(self):
        self.should_be_model_name()
        self.should_be_model_reference()
        self.should_be_model_big_image()
        self.should_be_short_description()
        self.should_be_order_details()

    def should_be_order_details(self):
        self.should_be_model_price()
        self.should_be_quantity_input()
        self.should_be_size_default()
        self.should_be_color_7_item()
        self.should_be_add_to_cart_button()
        self.should_be_add_to_wishlist_button()

    def should_be_model_url(self):
        assert 'id_product=4&controller=product' in self.driver.current_url

    def should_be_model_name(self):
        model_name = self.is_text_present(*ModelPageLocators.MODEL_NAME)
        if not model_name:
            pytest.fail('No text found')
        else:
            return model_name

    def should_be_model_reference(self):
        model_reference = self.is_text_present(*ModelPageLocators.MODEL_REFERENCE)
        if not model_reference:
            pytest.fail('No text found')
        else:
            return model_reference

    def should_be_model_big_image(self):
        model_big_image = self.is_element_present(*ModelPageLocators.MODEL_BIG_IMAGE)
        if not model_big_image:
            pytest.fail('No image found')
        else:
            return model_big_image

    def should_be_short_description(self):
        short_description = self.is_text_present(*ModelPageLocators.SHORT_DESCRIPTION)
        if not short_description:
            pytest.fail('No text found')
        else:
            return short_description

    def should_be_model_price(self):
        model_price = self.is_text_present(*ModelPageLocators.PRICE)
        if not model_price:
            pytest.fail('No text found')
        else:
            return model_price

    def should_be_quantity_input(self):
        quantity_input_default = self.is_element_present(*ModelPageLocators.QUANTITY_INPUT_FIELD)
        if not quantity_input_default:
            pytest.fail('No text found')
        else:
            return quantity_input_default

    def should_be_size_default(self):
        size_default = self.is_text_present(*ModelPageLocators.SIZE_DEFAULT_S)
        if not size_default:
            pytest.fail('No text found')
        else:
            return size_default

    def should_be_color_7_item(self):
        size_default = self.is_element_present(*ModelPageLocators.COLOR_7_ITEM)
        if not size_default:
            pytest.fail('No color item found')
        else:
            return size_default

    def should_be_add_to_cart_button(self):
        add_to_cart_button = self.is_element_present(*ModelPageLocators.ADD_TO_CART_BUTTON)
        if not add_to_cart_button:
            pytest.fail('No element found')
        else:
            return add_to_cart_button

    def add_model_to_cart(self):
        cart_button = self.should_be_add_to_cart_button()
        cart_button.click()

    def should_be_add_to_cart_success_alert(self):
        add_to_cart_success_alert = self.is_text_present(*ModelPageLocators.ADDED_TO_CART_SUCCESS_TEXT)
        if not add_to_cart_success_alert:
            pytest.fail('No text found')
        else:
            return add_to_cart_success_alert

    def should_be_add_to_wishlist_button(self):
        add_to_wishlist_button = self.is_element_present(*ModelPageLocators.ADD_TO_WISHLIST_BUTTON)
        if not add_to_wishlist_button:
            pytest.fail('No element found')
        else:
            return add_to_wishlist_button

    def add_model_to_wishlist(self):
        cart_button = self.should_be_add_to_wishlist_button()
        cart_button.click()

    def should_be_add_to_wishlist_success_alert(self):
        wishlist_success_alert = self.is_text_present(*ModelPageLocators.ADDED_TO_WISHLIST_SUCCESS_TEXT)
        if not wishlist_success_alert:
            pytest.fail('No text found')
        else:
            return wishlist_success_alert

    def should_be_should_be_go_to_cart_button(self):
        go_to_cart_button = self.is_element_present(*ModelPageLocators.GO_TO_CART_BUTTON)
        if not go_to_cart_button:
            pytest.fail('No element found')
        else:
            return go_to_cart_button

    def go_to_cart_from_alert(self):
        go_to_cart = self.should_be_should_be_go_to_cart_button()
        go_to_cart.click()

    def should_be_add_to_wishlist_fail_alert(self):
        wishlist_fail_alert = self.is_text_present(*ModelPageLocators.WISHLIST_FAILED_TEXT)
        if not wishlist_fail_alert:
            pytest.fail('No text found')
        else:
            return wishlist_fail_alert

