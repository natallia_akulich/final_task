from Task_1.pages.login_page import LoginPage
from Task_1.pages.main_page import MainPage
from Task_1.pages.cart_page import CartPage
from Task_1.pages.my_account_page import MyAccountPage
from Task_1.pages.model_page import ModelPage
from Task_1.xml_parser import get_test_data


url = get_test_data('main_page_url')
user_email = get_test_data('user_email')
user_password = get_test_data('user_password')


def test_user_can_open_main_page(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.should_be_main_page()


def test_user_can_open_login_page(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_login_page()
    login_page = LoginPage(driver, driver.current_url)
    login_page.should_be_login_page()


def test_user_can_open_cart_page_empty(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_cart_page()
    cart_page = CartPage(driver, driver.current_url)
    cart_page.should_be_cart_page()
    cart_page.empty_cart()


def test_user_can_login_successfully(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_login_page()
    login_page = LoginPage(driver, driver.current_url)
    login_page.login()
    my_account_page = MyAccountPage(driver, driver.current_url)
    my_account_page.should_be_my_account_page()


def test_user_login_empty_email_password(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_login_page()
    login_page = LoginPage(driver, driver.current_url)
    login_page.should_be_login_page()
    login_page.login_failed_empty_fields()


def test_user_login_error_empty_password(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_login_page()
    login_page = LoginPage(driver, driver.current_url)
    login_page.should_be_login_page()
    login_page.login_failed_empty_password()


def test_user_can_open_model_page(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.select_model_4()
    model_page = ModelPage(driver, driver.current_url)
    model_page.should_be_model_page()


def test_user_can_add_model_to_cart(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.select_model_4()
    model_page = ModelPage(driver, driver.current_url)
    model_page.should_be_model_page()
    model_page.add_model_to_cart()
    model_page.should_be_add_to_cart_success_alert()
    model_page.go_to_cart_from_alert()
    cart_page = CartPage(driver, driver.current_url)
    cart_page.should_be_cart_page()
    cart_page.should_be_product_table_if_not_empty()
    cart_page.model_4_added_to_cart()


def test_user_can_add_model_to_wishlist(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.open_login_page()
    login_page = LoginPage(driver, driver.current_url)
    login_page.login()
    my_account_page = MyAccountPage(driver, driver.current_url)
    my_account_page.should_be_my_account_page()
    my_account_page.return_home()
    main_page.select_model_4()
    model_page = ModelPage(driver, driver.current_url)
    model_page.should_be_model_page()
    model_page.add_model_to_wishlist()
    model_page.should_be_add_to_wishlist_success_alert()


def test_unauthorized_user_can_not_add_model_to_wishlist(driver):
    main_page = MainPage(driver, url)
    main_page.open()
    main_page.select_model_4()
    model_page = ModelPage(driver, driver.current_url)
    model_page.should_be_model_page()
    model_page.add_model_to_wishlist()
    model_page.should_be_add_to_wishlist_fail_alert()
