import xml.etree.ElementTree as ET

tree = ET.parse('data_for_tests.xml')
root = tree.getroot()


def get_test_data(key):
    for item in root.findall('doc/item'):
        value = item.get(key)
        if value:
            return value
