from selenium.webdriver.common.by import By


class ModelPageLocators:
    MODEL_NAME = (By.CSS_SELECTOR, 'div#center_column h1', 'Printed Dress')
    MODEL_REFERENCE = (By.CSS_SELECTOR, 'p#product_reference', 'Model demo_4')
    MODEL_BIG_IMAGE = (By.CSS_SELECTOR, 'img#bigpic')
    SHORT_DESCRIPTION = (By.CSS_SELECTOR, 'div#short_description_content',
                         'Printed evening dress with straight sleeves with '
                         'black thin waist belt and ruffled linings.')

    PRICE = (By.CSS_SELECTOR, 'span#our_price_display', '$50.99')
    QUANTITY_INPUT_FIELD = (By.CSS_SELECTOR, 'input#quantity_wanted')
    SIZE_DEFAULT_S = (By.CSS_SELECTOR, 'div#uniform-group_1 > span', 'S')
    COLOR_7_ITEM = (By.CSS_SELECTOR, 'a#color_7')
    ADD_TO_CART_BUTTON = (By.CSS_SELECTOR, 'p#add_to_cart span')
    ADDED_TO_CART_SUCCESS_TEXT = (By.XPATH, '//*[@id="layer_cart"]/div[1]/div[1]/h2',
                                  'Product successfully added to your shopping cart')
    ADD_TO_WISHLIST_BUTTON = (By.CSS_SELECTOR, 'a#wishlist_button')
    ADDED_TO_WISHLIST_SUCCESS_TEXT = (
        By.XPATH, '//*[@id="product"]/div[2]/div/div/div/div/p', 'Added to your wishlist.')
    WISHLIST_FAILED_TEXT = (
        By.CLASS_NAME, 'fancybox-error', 'You must be logged in to manage your wishlist.')
    GO_TO_CART_BUTTON = (By.CSS_SELECTOR, 'div#layer_cart a > span')



