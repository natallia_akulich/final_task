from selenium.webdriver.common.by import By


class MainPageLocators:
    LOGIN_LINK = (By.CSS_SELECTOR, 'a.login')
    CONTACT_US_LINK = (By.CSS_SELECTOR, 'div#contact-link > a')
    SEARCH_FIELD = (By.CSS_SELECTOR, 'input#search_query_top')
    SEARCH_BUTTON = (By.CSS_SELECTOR, 'form#searchbox > button[type="submit"]')
    CART_LINK = (By.CSS_SELECTOR, 'header#header div:nth-child(3) > div > a')
    TOP_MENU = (By.CSS_SELECTOR, 'div#block_top_menu > div')
    POPULAR_LINK = (By.CSS_SELECTOR, 'ul#home-page-tabs li.active > a')
    BEST_SELLERS_LINK = (
        By.CSS_SELECTOR, 'ul#home-page-tabs li:nth-child(2) > a')
    MODEL_4_LINK = (By.XPATH, '//*[@id="homefeatured"]/li[4]/div/div[2]/h5/a')

