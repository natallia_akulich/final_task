from selenium.webdriver.common.by import By


class CartPageLocators:
    CART_LINK = (By.CSS_SELECTOR, 'header#header div:nth-child(3) > div > a')

    CART_ITEMS_ZERO_AMOUNT = (
        By.CSS_SELECTOR, 'span.ajax_cart_no_product', '(empty)')
    EMPTY_CART_WARNING = (
        By.CSS_SELECTOR, 'p.alert.alert-warning',
        'Your shopping cart is empty.')

    SUMMARY_STEP = (By.CSS_SELECTOR, 'li.step_current.first')
    SIGN_IN_STEP = (By.CSS_SELECTOR, 'li.step_todo.second')
    ADDRESS_STEP = (By.CSS_SELECTOR, 'li.step_todo.third')
    SHIPPING_STEP = (By.CSS_SELECTOR, 'li.step_todo.four')
    PAYMENT_STEP = (By.CSS_SELECTOR, 'li#step_end')

    TABLE_PRODUCT = (By.CSS_SELECTOR, 'table#cart_summary th.cart_product.first_item')
    TABLE_DESCRIPTION = (By.CSS_SELECTOR, 'table#cart_summary th.cart_description.item')
    TABLE_AVAILABLE = (By.CSS_SELECTOR, 'table#cart_summary th.cart_avail.item')
    TABLE_UNIT_PRICE = (By.CSS_SELECTOR, 'table#cart_summary th.cart_unit.item')
    TABLE_QUANTITY = (By.CSS_SELECTOR, 'table#cart_summary th.cart_quantity.item')
    TABLE_TOTAL = (By.CSS_SELECTOR, 'table#cart_summary th.cart_total.item')
    TABLE_DELETE = (By.CSS_SELECTOR, 'table#cart_summary th.cart_delete.last_item')

    MODEL_4_IN_CART = (By.CSS_SELECTOR, 'table#cart_summary small.cart_ref', 'demo_4')
