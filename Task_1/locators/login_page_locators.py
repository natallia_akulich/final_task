from selenium.webdriver.common.by import By


class LoginPageLocators:
    REGISTER_FORM = (By.CSS_SELECTOR, "form#create-account_form")
    REGISTER_FORM_EMAIL_FIELD = (By.CSS_SELECTOR, 'input#email_create')
    REGISTER_FORM_CREATE_BUTTON = (By.CSS_SELECTOR, 'button#SubmitCreate')

    LOGIN_FORM = (By.CSS_SELECTOR, "form#login_form")
    LOGIN_FORM_EMAIL_FIELD = (By.CSS_SELECTOR, 'input#email')
    LOGIN_FORM_PASSWORD_FIELD = (By.CSS_SELECTOR, 'input#passwd')
    LOGIN_FORM_FORGOT_PASSWORD = (By.CSS_SELECTOR, 'form#login_form a')
    LOGIN_FORM_SIGN_BUTTON = (By.CSS_SELECTOR, 'button#SubmitLogin')

    ALERT = (By.CSS_SELECTOR, 'div#center_column > div.alert.alert-danger')
    ALERT_ONE_ERROR_TEXT = (By.CSS_SELECTOR, 'div#center_column div.alert.alert-danger > p',
                            'There is 1 error')
    ALERT_EMAIL_ERROR_TEXT = (By.CSS_SELECTOR, 'div#center_column li', 'An email address required.')
    ALERT_PASSWORD_ERROR_TEXT = (By.CSS_SELECTOR, 'div#center_column li', 'Password is required.')
    ALERT_AUTH_FAILED = (By.CSS_SELECTOR, 'div#center_column li', 'Authentication failed.')

