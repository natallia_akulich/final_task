from selenium.webdriver.common.by import By


class MyAccountPageLocators:
    SIGN_OUT_BUTTON = (By.CLASS_NAME, 'logout')
    MY_ACCOUNT_BUTTON = (By.CLASS_NAME, 'header#header nav > div:nth-child(1) > a > span')
    MY_ACCOUNT_HEADING = (By.CSS_SELECTOR, 'div#center_column > h1', 'MY ACCOUNT')

    ACCOUNT_INFO_TEXT = (By.CLASS_NAME, 'info-account',
                         'Welcome to your account. Here you can manage '
                         'all of your personal information and orders.')
    ORDER_HISTORY_BUTTON = (By.CSS_SELECTOR,
                            'div#center_column div:nth-child(1) > ul > li:nth-child(1) > a > span',
                            'ORDER HISTORY AND DETAILS')
    MY_CREDIT_SLIPS_BUTTON = (By.CSS_SELECTOR, 'div#center_column li:nth-child(2) > a > span',
                              'MY CREDIT SLIPS')
    MY_ADDRESSES_BUTTON = (By.CSS_SELECTOR, 'div#center_column li:nth-child(3) > a > span',
                           'MY ADDRESSES')
    MY_PERSONAL_INFORMATION_BUTTON = (By.CSS_SELECTOR, 'div#center_column li:nth-child(4) > a > span',
                                      'MY PERSONAL INFORMATION')
    MY_WISHLISTS_BUTTON = (By.CSS_SELECTOR, 'div#center_column div:nth-child(2) > ul > li > a > span',
                           'MY WISHLISTS')

    RETURN_HOME_BUTTON = (By.CSS_SELECTOR, 'div#columns div.breadcrumb.clearfix > a')



