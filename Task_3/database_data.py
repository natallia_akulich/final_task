import mysql.connector as mysql
from mysql.connector import Error
from Task_3 import data


def create_database(host, user, password, database):
    db = mysql.connect(host=host, user=user, passwd=password)
    cursor = db.cursor()
    try:
        cursor.execute(f"CREATE DATABASE {database}")
        print(f'Database {database} was created successfully!')
    except Error as error:
        print(f'Database creation failed: {error}')

    db.close()


def connect_to_db(host, user, password, database):
    connect = None
    try:
        connect = mysql.connect(
            host=host, user=user, passwd=password, database=database)
        print("Connected successfully!")
    except Error as error:
        print(f'Connection failed: {error}')

    return connect


def close_connect_to_db(connection):
    connection.close()


def create_table(connection, table, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print(f'Table {table} was created successfully!')
    except Error as error:
        print(f'Table creation failed: {error}')


def update_table(connection, table, query, values):
    cursor = connection.cursor()
    try:
        cursor.executemany(query, values)
        connection.commit()
        print(f'Table {table} was updated successfully, '
              f'{cursor.rowcount}, records inserted')
    except Error as error:
        print(f'Update table failed: {error}')


def execute_query(connection, query):
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        print('Connected successfully! Select results: ')
        return result
    except Error as error:
        print(f'Execute query failed: {error}')


def run():
    create_database('localhost', 'root', data.password, data.database_name)
    db_connection = connect_to_db('localhost', 'root', data.password, data.database_name)

    create_table(db_connection, 'vehicles', data.vehicles_table_create_query)
    update_table(db_connection, 'vehicles', data.vehicles_insert_query, data.vehicles_table_values)

    create_table(db_connection, 'customers', data.customers_table_create_query)
    update_table(db_connection, 'customers', data.customers_insert_query, data.customers_table_values)

    create_table(db_connection, 'rent_history', data.rent_history_table_create_query)
    update_table(db_connection, 'rent_history', data.rent_history_insert_query, data.rent_history_table_values)

    result_1 = execute_query(db_connection, data.select_vehicles_model_license_plate_query)
    print(result_1)
    result_2 = execute_query(db_connection, data.select_vehicles_except_nissan_query)
    print(result_2)
    result_3 = execute_query(db_connection, data.select_vehicles_with_mileage)
    print(result_3)
    update_table(db_connection, 'vehicles', data.new_vehicles_insert_query, data.add_vehicles_table_values)

    close_connect_to_db(db_connection)


if __name__ == '__main__':
    run()
