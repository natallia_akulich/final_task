import docker
import traceback
from mysql.connector import Error
from Task_3 import database_data, data
import time


if __name__ == '__main__':
    image_name = 'akulich/db'

    client = docker.from_env()
    client.images.pull('mysql:latest')
    client.images.build(path='.', tag=image_name)
    container = client.containers.run(image='mysql:latest',
                                      environment=[f'MYSQL_ROOT_PASSWORD={data.password}'],
                                      ports={'3306/tcp': 3306}, detach=True)

    try:
        time.sleep(20)
        database_data.run()
    except Error:
        print(traceback.format_exc())

    container.stop()
