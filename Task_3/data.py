database_name = 'Car_sharing'
password = 'Your_password'

vehicles_table_create_query = 'CREATE TABLE vehicles ' \
                            '(license_plate VARCHAR(255) NOT NULL PRIMARY KEY, ' \
                            'type VARCHAR(255), ' \
                            'model VARCHAR(255), ' \
                            'description VARCHAR(255), ' \
                            'rent_price VARCHAR(255), ' \
                            'transmission VARCHAR(255),' \
                            'mileage VARCHAR(255), ' \
                            'drive_type VARCHAR(255))'

vehicles_table_values = [('1542PM7', 'sedan', 'nissan', 'Very good condition', '200$', 'manual', '200', 'RWD'),
                         ('1543PM7', 'coupe', 'bmw', 'Very good condition', '400$', 'auto', '100', 'FWD'),
                         ('1544PM7', 'minivan', 'audi', 'Very good condition', '400', 'auto', '50', 'AWD'),
                         ('1545PM7', 'coupe', 'audi', 'Very good condition', '600', 'auto', '20', 'RWD'),
                         ('9042PP7', 'sedan', 'nissan', 'Very good condition', '200$', 'manual', '140', 'RWD'),
                         ('9142PP7', 'coupe', 'bmw', 'Very good condition', '400$', 'auto', '120', 'FWD'),
                         ('9242PP7', 'sedan', 'renault', 'Very good condition', '250', 'manual', '90', 'FWD'),
                         ('9342PP7', 'minivan', 'nissan', 'Very good condition', '300$', 'manual', '80', 'FWD'),
                         ('3042HH7', 'sedan', 'renault', 'Very good condition', '200', 'manual', '200', 'AWD'),
                         ('3142HH7', 'hatchback', 'bmw', 'Very good condition', '450$', 'auto', '70', 'AWD'),
                         ('3242HH7', 'sedan', 'audi', 'Very good condition', '500', 'auto', '140', 'AWD'),
                         ('3342HH7', 'hatchback', 'bmw', 'Very good condition', '450$', 'auto', '130', 'RWD')]
vehicles_insert_query = 'INSERT INTO vehicles ' \
                      '(license_plate, type, model, description, ' \
                      'rent_price, transmission, mileage, drive_type) ' \
                      'VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'

customers_table_create_query = 'CREATE TABLE customers ' \
                            '(customer_id INT NOT NULL PRIMARY KEY, ' \
                            'name VARCHAR(255), ' \
                            'phone VARCHAR(255), ' \
                            'address VARCHAR(255))'

customers_table_values = [(50131, 'Artem', '701857944556', 'Lenina-13-7'),
                          (50132, 'Svetlana', '70183344333', 'Krasnaya-77-99'),
                          (50133, 'Ekaterina', '70183366776', 'Belaya-22-145'),
                          (50134, 'Mihail', '70183349873', 'Centralnaya-29-14'),
                          (50135, 'Valeriy', '70183348888', 'Pobedy-43-176'),
                          (50136, 'Alexander', '70189977556', 'Belaya-48-199'),
                          (50137, 'Mariya', '70183340011', 'Lenina-10-22')]
customers_insert_query = 'INSERT INTO customers (customer_id, name, phone, address) VALUES (%s, %s, %s, %s)'

rent_history_table_create_query = 'CREATE TABLE rent_history ' \
                            '(rent_id INT NOT NULL PRIMARY KEY, ' \
                            'customer_id INT NOT NULL, ' \
                            'license_plate VARCHAR(255) NOT NULL, ' \
                            'rent_start_date VARCHAR(255), ' \
                            'rent_end_date VARCHAR(255))'

rent_history_table_values = [(1010, 50131, '1543PM7', '11-03-2020', '12-03-2020'),
                             (1011, 50132, '3342HH7', '15-02-2020', '16-02-2020'),
                             (1012, 50135, '3142HH7', '10-03-2020', '12-03-2020'),
                             (1013, 50137, '1544PM7', '14-05-2020', '20-05-2020'),
                             (1014, 50136, '1545PM7', '01-05-2020', '07-05-2020'),
                             (1015, 50134, '3342HH7', '18-04-2020', '22-04-2020'),
                             (1016, 50133, '9242PP7', '22-03-2020', '28-03-2020')]
rent_history_insert_query = 'INSERT INTO rent_history ' \
                                   '(rent_id, customer_id, license_plate, rent_start_date,' \
                                   'rent_end_date) VALUES (%s, %s, %s, %s, %s)'

select_vehicles_model_license_plate_query = 'SELECT model, license_plate FROM vehicles'
select_vehicles_except_nissan_query = 'SELECT model, license_plate FROM vehicles WHERE model != "nissan"'
select_vehicles_with_mileage = 'SELECT model, license_plate FROM vehicles WHERE mileage BETWEEN 100 AND 150'

add_vehicles_table_values = [('5152AA9', 'hatchback', 'fiat', 'Very good condition', '150$', 'manual', '100', 'RWD'),
                             ('7865AA9', 'hatchback', 'bmw', 'Very good condition', '200$', 'auto', '900', 'RWD')]
new_vehicles_insert_query = 'INSERT INTO vehicles ' \
                      '(license_plate, type, model, description, ' \
                      'rent_price, transmission, mileage, drive_type) ' \
                      'VALUES (%s, %s, %s, %s, %s, %s, %s, %s)'
